.MODEL SMALL
.STACK 100H
.DATA  
STRING DB 100 DUP(?)
out1 DB "Masukkan kalimat: $"
out2 DB 0DH, 0AH, "Jumlah Kalimat Yang Ditemukan: $"
WORDS DW 0     
.CODE
MAIN PROC
    
     MOV AX, @DATA
     MOV DS, AX
     MOV ES, AX
     
     MOV AH, 9
     LEA DX, out1
     INT 21H
     
     LEA DI, STRING
 
     MOV AH, 1
     READ:
     INT 21H
     CMP AL, 0DH
     JE AKHIRKALIMAT
     STOSB
     JMP READ
     
     AKHIRKALIMAT:
     MOV AL, "$"
     STOSB
     
     XOR BX, BX
     
     COUNT:
     MOV AL, STRING[BX]
     CMP AL, "$"
     JE EXIT
     CMP AL, " "
     JE KALIMAT_SELESAI      
     INC BX
     JMP COUNT
     
     KALIMAT_SELESAI:
     INC WORDS
     INC BX 
     JMP COUNT     
         
     EXIT:
     INC WORDS
     
     MOV AH, 9
     LEA DX, out2
     INT 21H
     
     MOV AH, 2
     MOV DX, WORDS
     ADD DX, 30H
     INT 21H
     
    MAIN ENDP
END MAIN